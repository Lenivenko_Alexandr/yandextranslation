package com.lenivenco.as.yandexnetworktranslation;

import android.app.LoaderManager;
import android.content.Context;
import android.content.Intent;
import android.content.Loader;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import java.util.zip.Inflater;

public class TranslateActivity extends AppCompatActivity implements LoaderManager.LoaderCallbacks<String> {
    private static final int TRANSLATE_LOADER_ID = 1;

    private final String YANDEX_REQUEST_URL = "https://translate.yandex.net/api/v1.5/tr.json/translate?";
    private final String KEY = "trnsl.1.1.20170206T202707Z.a33b20d6b1047f83.f69b27c5c63d1afb97a4105706278bec15fa1986";

    private View mProgressBar;
    private TextView mResultTextView;
    private EditText mInputTextEditText;
    private TranslateLoader mTranslataLoader = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_translate);


        mResultTextView = (TextView) findViewById(R.id.resultTextViewTranslateActivity);

        mProgressBar = findViewById(R.id.progressBar);

        mInputTextEditText = (EditText) findViewById(R.id.inputWordEditTextTranslateActivity);

        View translateButton = findViewById(R.id.searchButtonTranslateActivity);
        translateButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!TextUtils.isEmpty(mInputTextEditText.getText().toString())){
                    // Get a reference to the LoaderManager, in order to interact with loaders.
                    android.app.LoaderManager loaderManager = getLoaderManager();
                    if(mTranslataLoader!=null){

                        loaderManager.destroyLoader(TRANSLATE_LOADER_ID);
                    }

                    // Initialize the loader. Pass in the int ID constant defined above and pass in null for
                    // the bundle. Pass in this activity for the LoaderCallbacks parameter (which is valid
                    // because this activity implements the LoaderCallbacks interface).
                    mTranslataLoader = (TranslateLoader) loaderManager.initLoader(TRANSLATE_LOADER_ID, null, TranslateActivity.this);

                }

            }
        });

        if(!isOnline()){
            Toast.makeText(this,R.string.no_internet_connection_messega,Toast.LENGTH_LONG).show();
            translateButton.setClickable(false);
        }

        setLanguageTitle();
    }

    private void setLanguageTitle() {
        TextView languageTextView = (TextView) findViewById(R.id.languageTextViewTranslateActivity);
        String lang = null;
        switch (getLanguage()){
            case "en-ru":
                lang = getResources().getString(R.string.settings_en_ru_label);
                break;
            case "ru-en":
                lang = getResources().getString(R.string.settings_ru_en_label);
                break;

        }
        languageTextView.setText(lang);
        languageTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startSettingActivity();
            }
        });
    }

    /*Method return true if device has internet connection*/
    public boolean isOnline() {
        ConnectivityManager cm =
                (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        return netInfo != null && netInfo.isConnectedOrConnecting();
    }
/*public final static String YANDEX_REQUEST_URL = "https://translate.yandex.net/api/v1.5/tr.json/translate?key=" +
            "trnsl.1.1.20170206T202707Z.a33b20d6b1047f83.f69b27c5c63d1afb97a4105706278bec15fa1986" +
            "&text=hello+World" +
            "&lang=en-ru";*/

    @Override
    public Loader<String> onCreateLoader(int id, Bundle args) {
        mProgressBar.setVisibility(View.VISIBLE);

        String language = getLanguage();

        Uri baseUri = Uri.parse(YANDEX_REQUEST_URL);
        Uri.Builder uriBuilder = baseUri.buildUpon();

        uriBuilder.appendQueryParameter("key", KEY);
        uriBuilder.appendQueryParameter("text", mInputTextEditText.getText().toString());
        uriBuilder.appendQueryParameter("lang", language);

        return  new TranslateLoader(this,uriBuilder.toString());
    }

    @Override
    public void onLoadFinished(Loader<String> loader, String data) {
        mProgressBar.setVisibility(View.INVISIBLE);
        mResultTextView.setText(data);

    }

    @Override
    public void onLoaderReset(Loader<String> loader) {

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            startSettingActivity();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void startSettingActivity() {
        Intent settingsIntent = new Intent(this, SettingActivity.class);
        startActivity(settingsIntent);
    }

    public String getLanguage() {
        SharedPreferences sharedPrefs = PreferenceManager.getDefaultSharedPreferences(this);

        String language = sharedPrefs.getString(
                getString(R.string.settings_language_key),
                getString(R.string.settings_language_default)
        );

        return language;
    }
}
